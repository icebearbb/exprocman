<?php
	include 'dbinterface.inc.php';
	include 'usermgr.inc.php';
	session_start();
	
	DB::connect();
	$u=User::auth_ok();
	if (!isset($u)) {
		header('Location: logout.php');
		exit;
	}
	
	include 'header.inc.php';
	echo Header::generate ($u);
	include 'menu.inc.php';
	echo Menu::generate ($u,'desktop');
	include 'view.inc.php';
	
	$v=new VorgangListView($u);
	echo $v->html();
	echo '<br><br><br><br>';
	
	include 'footer.inc.php';
	echo Footer::generate ($u);
?>
