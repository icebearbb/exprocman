<?php

if (!class_exists("Vorgang"))
	include 'view.inc.php';

class Edge{
	public $vra=-1;
	public $vrb=-1;
	public function __construct($a,$b){
		$this->vra=$a;
		$this->vrb=$b;
	}
}

class VRGraph {

	private $edges=array();
	private $vgid=-1;

	public static function from_vg($vgid) {
		$vgr=new VRGraph();
		$vgr->set_vgid($vgid);
		$vg=new Vorgang($vgid,NULL);
		$edg=DB::get_assoc('SELECT vrid_vor,vrid_nach FROM vrgraph
			WHERE vrid_vor IN ('.implode(',',$vg->vrids).')');
		foreach ($edg as $e) {
			$vgr->add_edge($e['vrid_vor'],$e['vrid_nach']);
		}
		return $vgr;
	}
	
	public function add_edge($a,$b) {
		$this->edges[]=new Edge($a,$b);
	}
	
	public function set_vgid($v) {
		$this->vgid=$v;
	}
	
	public function commit() {
		$vg=new Vorgang($this->vgid,NULL);
		DB::query('DELETE FROM vrgraph WHERE vrid_vor IN ('.implode(',',$vg->vrids).')');
		foreach ($this->edges as $e) {
			DB::query("INSERT INTO vrgraph (vrid_vor,vrid_nach) VALUES ($e->vra,$e->vrb)");
		}
	}
	
	public function get_succ($vrid) {
		$res=array();
		foreach ($this->edges as $e) {
			if ($e->vra==$vrid) $res[]=$e->vrb;
		}
		return $res;
	}
	
	public function get_pred($vrid) {
		$res=array();
		foreach ($this->edges as $e) {
			if ($e->vrb==$vrid) $res[]=$e->vra;
		}
		return $res;
	}

}

?>