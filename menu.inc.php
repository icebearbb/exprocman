<?php
class Menu {
	
	static function generate($u,$page) {
		$res='<div class="mu"><ul><li><b style="font-family:Arial;font-size:1em;">PROCman</b></li>';
		$res.=Menu::text($u->vorname.' '.$u->name);
		switch ($page) {
			case 'desktop':$res.=Menu::menu_desktop($u); break;
			case 'vorgang':
			case 'gpadmin':
			case 'newvg':$res.=Menu::menu_default($u); break;
		}
		$res.=Menu::link('Abmelden','logout.php');
		$res.='</ul></div><br><br>';
		return $res;
	}
	
	static function text($text) {
		return '<li><b style="color:#0000CC;">'.$text.'</b></li>';
	}
	
	static function link($text,$link) {
		return '<li><a href="'.$link.'">'.$text.'</a>'."\n";
	}
	
	static function menu_desktop($u) {
		return ($u->gid!='2'?Menu::link('Neue 5PK Prüfung','newvg.php'):'').
			//($u->gid!='2'?Menu::link('Neue MSA Prüfung','newmsa.php'):'').
			($u->gid=='1'?Menu::link('5PK Gruppenprüfungen','gpadmin.php'):'').
			($u->gid=='1'?Menu::link('Export','export.php'):'').
			Menu::menu_inbox($u);
	}
	
	static function menu_default($u) {
		return Menu::link('Zurück','desktop.php');
	}
	
	static function menu_inbox($u) {
		return '';
		//return Menu::link('Nachrichten','inbox.php');
	}
}
?>
