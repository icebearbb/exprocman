<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

require './vendor/autoload.php';

include 'config.mail.php';

class Mail {
	
	static $lasterror='';
	
	static function sendcron($to,$name,$subj,$body) {
		$body='Liebe(r) '.$name."!\n\n".$body.
			"\n\n".'Viele Gruesse vom Humboldt-Pruefungsmanager!';
	
		Mail::sendm($to,$name,$subj,$body);
	}
	
	static function send ($u, $subj, $body) {
		
		$body='Liebe(r) '.$u->vorname.' '.$u->name."!\n\n".$body.
			"\n\n".'Viele Gruesse vom Humboldt-Pruefungsmanager!';
	
		Mail::sendm($u->mail,$u->vorname.' '.$u->name,$subj,$body);
		
	}
	
	static function sendm ($addr, $name, $subj, $body) {
		
		if (!class_exists("Tools"))
			include 'tools.inc.php';
		try {
			$mail = new PHPMailer();
			$mail->isSMTP();
			$mail->SMTPDebug = 0;
			$mail->Host = MailConfig::$host;
			$mail->Port = MailConfig::$port;
			$mail->SMTPSecure = 'tls';
			$mail->SMTPAuth = MailConfig::$smtp_auth;
			$mail->Username = MailConfig::$username;
			$mail->Password = MailConfig::$password;
			//$mail->SMTPSecure = 'ssl';
			$mail->setFrom(MailConfig::$from_address, MailConfig::$from_name);
			$mail->addReplyTo(MailConfig::$reply_address, MailConfig::$reply_name);
			$mail->addAddress($addr, $name);
			$mail->Subject = $subj;
			$mail->Body =$body;
			$mail->send();
			Tools::log('mail',"sent mail to $addr");
		} catch (Exception $e) {
			echo "Mailer Error: {$mail->ErrorInfo}";
		}
	}
}

?>
