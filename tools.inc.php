<?php

if (!class_exists("Voraussetzung"))
	include 'view.inc.php';
if (!class_exists("VRGraph"))
	include 'vrgraph.inc.php';

if (!class_exists("Tools")) {
class Tools {
	
	const DEBUG=true;
	
	static function gentable ($data) {
		$res="<table>\n";
		$cols=count($data[0]);
		foreach ($data as $row) {
			$res.='<tr>';
			foreach ($row as $cell) {
				$res.='<td>'.$cell.'</td>';
			}
			$res.="</tr>\n";
		}
		$res.="</table>\n";
		return $res;
	}
	
	static function get_fl_pid ($fach) {
		return DB::get_value("SELECT pid FROM hatfach WHERE fid='$fach' AND fl=1");
	}
	
	static function get_sl_pid () {
		return 'j.kayser';
		//return DB::get_value("SELECT pid FROM person WHERE gid=4");
	}
	
	static function check_email_address($email) {
		$email = trim($email);
		$email = stripslashes($email);
		$email = htmlspecialchars($email);
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			return false;
		}
		return true;
	}
	
	private static function set_succ($vr,$u,$vrg,$val) {
		$nx=$vrg->get_succ($vr->vrid);
		foreach ($nx as $n) {
			$vrnx=new Voraussetzung($n,$u);
			Tools::log('set_succ',$vrnx->name.' request '.$val);
			Tools::chg_sta($vrnx,$u,$vrg,$val);
		}
	}
	
	static function change_status($vr,$u,$val){
		$vrg=VRGraph::from_vg($vr->vgid);
		Tools::chg_sta($vr,$u,$vrg,$val);
	}
	
	static function change_cascade ($vr,$u,$vrg,$val) {
		Tools::log('chg_cas',$vr->name.' request '.$val);
		if ($val=='lock' || $val=='reject') {
			if ($vr->status=='open') {
				Tools::log('chg_cas',$vr->name.' is open, request lock');
				$preds=$vrg->get_pred($vr->vrid);
				foreach ($preds as $predid) {
						$p=new Voraussetzung($predid,$u);
						$p->status='open';
						$p->commit();
						Tools::log('chg_cas',$p->name.' set to open');
					}
			}
			$vr->status='lock';
			$vr->commit();
			Tools::log('chg_cas',$vr->name.' set to lock');
			Tools::set_succ($vr,$u,$vrg,'lock');
			return;
		}
		if ($val=='toggle') {
			if ($vr->status=='open')
				$val='ok';
			else 
				$val='open';
		}
		$pred=$vrg->get_pred($vr->vrid);
		$staok=true;
		foreach ($pred as $p) 
			if (Voraussetzung::get_status($p)!='ok') {
				$staok=false;
				break;
			}
		if ($staok) {
			$vr->status=$val;
			$vr->commit();
			Tools::log('chg_cas','staok true, '.$vr->name.' set to '.$val);
			if ($val=='ok') {
				Tools::set_succ($vr,$u,$vrg,'open');
			} else {
				Tools::set_succ($vr,$u,$vrg,'lock');
			}
		} else { 
			$vr->status='lock';
			$vr->commit();
			Tools::log('chg_cas','staok false, '.$vr->name.' set to lock');
			Tools::set_succ($vr,$u,$vrg,'lock');
		}
	}
	
	static function chg_sta($vr,$u,$vrg,$val) { // Voraussetzung, User, VRGraph, string
		Tools::log('chg_sta',$vr->name.' request '.$val);
		if ($vr->status==$val) {
			Tools::log('chg_sta',$vr->name.' is '.$val.' already');
			return;
		}
		switch ($vr->type) {
		case 'mail':
		case 'bfach':
			Tools::change_cascade($vr,$u,$vrg,$val);
			break;
		case 'flref':
		case 'flbez': 
			if ($val!='reject')
				Tools::change_cascade($vr,$u,$vrg,$val);
			else {
				$vr=new Vorgang ($vr->vgid,$u);
				foreach ($vr->vrids as $vrid) {
					$vg=new Voraussetzung($vrid,$u);
					switch ($vg->type) {
						case 'info':
						case 'con':
							$vg->status='open';
							$vg->commit();
							break;
						case 'accept':
						case 'flref':
						case 'flbez':
						case 'sl':
							$vg->status='lock';
							$vg->commit();
							break;
					}
				}
			}
			break;
		case 'sl':
			if ($val!='reject')
				Tools::change_cascade($vr,$u,$vrg,$val);
			else {
				$vr=new Vorgang ($vr->vgid,$u);
				foreach ($vr->vrids as $vrid) {
					$vg=new Voraussetzung($vrid,$u);
					switch ($vg->type) {
						case 'info':
						case 'con':
							$vg->status='open';
							$vg->commit();
							break;
						case 'accept':
						case 'flref':
						case 'flbez':
						case 'sl':
							$vg->status='lock';
							$vg->commit();
							break;
					}
				}
			}
			break;
		case 'info':
		case 'con':
		case 'accept':
			Tools::change_cascade($vr,$u,$vrg,$val);
			break;
		}
	}
	
	static function log($tag,$data) {
		if (!Tools::DEBUG) return;
		$fp = fopen('proc.log', 'a');
		fwrite($fp, date("Y-m-d H:i:s").': '.$tag.': '.$data."\n");
		fclose($fp);
	}
	
	static function genpwd() {
		$ziffern = "abcdefghijklmnopqrstuvwxyz123456789WERTZUPLKJHGFDSAYXCVBNM";
		$passwort="";
		for($i = 0; $i < 8; $i++) {
			$passwort .= substr($ziffern,(rand()%(strlen ($ziffern))), 1);
		}
		return $passwort;
	}
	
	static function data_empty($keys,$data) {
		foreach ($keys as $k) {
			Tools::log('data_empty',strlen(trim ($data[$k])).' '.trim ($data[$k]));
			if (strlen(trim ($data[$k]))<10) return true;
		}
		return false;
	}

	static function xssprotect($s) {
		$s=htmlspecialchars($s,ENT_COMPAT|ENT_XHTML,'utf-8');
		$s=nl2br($s);
		return $s;
	}
	
	static function fix($str) {
		/*$str=nl2br($str);
		$str=str_replace(' />','>',$str);*/
		$str=stripslashes($str);
		$str=str_replace(chr(92),'',$str);
		$str=str_replace("\r\n",'\\\\n',$str);
		$str=str_replace('&quot;','##',$str);
		$str=str_replace('&quot;','##',$str);
		$str=str_replace('(','',$str);
		$str=str_replace(')','',$str);
		//$str=str_replace(',','',$str);
		return $str;
	}
	
}

} // if !class_exists

?>