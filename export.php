<?php

	function fixup($str) {
		$str=preg_replace("/\*\*/",'\'',$str);
		$str=preg_replace("/\&quot;/",'',$str);
		$str=preg_replace("/&lt;/",'<',$str);
		$str=preg_replace("/&gt;/",'>',$str);
		$str=preg_replace("/&amp;/",'&',$str);
		//$str=preg_replace("/\r\n/","\n",$str);
		$str=preg_replace("/:rn/",":\n",$str);
		$str=preg_replace("/ rn/","\n",$str);
		$str=preg_replace("/\\\/",'',$str);
		return $str;
	}

	include 'dbinterface.inc.php';
	include 'usermgr.inc.php';
	session_start();
	
	DB::connect();
	$u=User::auth_ok();
	if (!isset($u)) {
		header('Location: logout.php');
		exit;
	}
	
	if (!$u->gid==1)
		die ('not allowed');
	
	if (isset($_POST) && count($_POST)>0) {
		include 'view.inc.php';
		include 'tools.inc.php';
		//header("Content-type: text/csv");
		//header("Content-Disposition: attachment; filename=pruefungen.csv");
		//header("Pragma: no-cache");
		//header("Expires: 0");
		echo '#prfnum#$#snum#$#name#$#vorname#$#pruefer#$#prfname#$#prfvorname#$#ref#$#bez#$#thema#$#leitfrage#$'.
			'#motivation#$#material#$#fachueber#$#abstract#'."\n";
		foreach (array_keys($_POST) as $vgid) {
			if (!is_numeric($vgid)) continue;
			$vg=new Vorgang($vgid,$u);
			$data=DB::get_assoc('SELECT typ,inhalt,vrid FROM voraus WHERE vrid IN ('.
				implode(',',$vg->vrids).")");
			foreach ($data as $d) {
				if ($d['typ']!='con' && $d['typ']!='info') continue;
				$texts=DB::get_assoc("SELECT vkey,vdata FROM vrdata WHERE vrid=".$d['vrid']);
				$keys=array();
				if ($d['typ']=='con') {
					$keys=array('motivation','material','intersub','abstract');
					$longinfo=Voraussetzung::transpose($keys,$texts);
				}
				if ($d['typ']=='info') {
					$keys=array('topic','quest');
					$shortinfo=Voraussetzung::transpose($keys,$texts);
				}		
				
			}
			$stu=new User($vg->get_stud_pid(),2);
			$exm=DB::get_value("SELECT DISTINCT pid FROM darf d, voraus vr ".
				"WHERE d.vrid=vr.vrid AND vr.vgid=$vgid AND d.role='exm'");
			$exmname=DB::get_assoc_row("SELECT name,vorname FROM person WHERE pid='$exm'");
			$linedata=array(
				$vgid,
				$stu->pid,
				$stu->name,
				$stu->vorname,
				$exm,
				$exmname['name'],
				$exmname['vorname'],
				$stu->rfach,
				$stu->bfach,
				$shortinfo['topic'],
				$shortinfo['quest'],
				$longinfo['motivation'],
				$longinfo['material'],
				$longinfo['intersub'],
				$longinfo['abstract']);
			echo '#'.implode('#$#',$linedata)."#\n";
			DB::query("UPDATE vorgang SET export=NOW() WHERE vgid=$vgid");
		}
		exit();
	}
	
	include 'header.inc.php';
	echo Header::generate ($u);
	include 'menu.inc.php';
	echo Menu::generate ($u,'gpadmin');
	include 'view.inc.php';
	
	if (!isset($_GET['all'])) {
		$vgs=DB::get_assoc("SELECT v.vgid,v.export FROM vorgang v, voraus vr, darf d WHERE v.vgid=vr.vgid AND d.vrid=vr.vrid AND d.role='sl' AND vr.status='ok' ORDER BY v.vgid ASC");
	} else {
		$vgs=DB::get_assoc("SELECT DISTINCT v.vgid FROM voraus v, darf d where v.vrid=d.vrid and d.pid BETWEEN '00000' AND '99999'");
	}
	
	echo 
		'<button onclick="
			for (i=0;i<document.forms[0].elements.length;i++) {
				e=document.forms[0].elements[i];
				if (e.name==parseInt(e.name))
					e.checked=true;
			}
			return false;
		">Alle Haken setzen</button>&nbsp;&nbsp;'.
		'<button onclick="
			for (i=0;i<document.forms[0].elements.length;i++) {
				e=document.forms[0].elements[i];
				if (e.name==parseInt(e.name))
					e.checked=false;
			}
			return false;
		">Alle Haken löschen</button>&nbsp;&nbsp;'.
		'<button onclick="document.location=\'?all\'">Alle Vorgänge</button>&nbsp;&nbsp;'.
		'<button onclick="document.location=\'?\'">Nur vollständige Vorgänge</button>'.
		'<form action="export.php" method="POST">'.
		'<input type="submit" value="Auswahl exportieren">'.
		'<table class="desktop"><tr><th>Name</th><th>Export</th></tr>';
	foreach ($vgs as $v) {
		$vg=new Vorgang($v['vgid'],$u);
		$name=$vg->get_stud_name();
		echo '<tr><td>'.$name.'</td><td>'.
		'<input type="checkbox" '.($vg->export==0?'checked':'').' name="'.$vg->vgid.'">'.
		'</td></tr>'."\n";
		
	}
	
	echo '</table>'.
	'<input type="hidden" name="doit"></form>';
	
	include 'footer.inc.php';
	echo Footer::generate ($u);
		
?>