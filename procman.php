<?php
	include 'dbinterface.inc.php';
	include 'usermgr.inc.php';
	session_start();
	
	DB::connect();
	$u=User::auth_ok();
	if (!isset($u)) {
		header('Location: logout.php');
		exit;
	}
	
	if (!$u->gid==1)
		die ('not allowed');
	
	if (isset($_GET['delvg']) && $u->gid==1) {
		$vgid=DB::sqli()->escape_string($_GET['delvg']);
		$vrids=DB::get_list("SELECT vrid FROM voraus WHERE vgid=$vgid");
		$studpid=DB::get_value('SELECT DISTINCT pid FROM darf WHERE vrid='.$vrids[0]." AND role='stu'");
		DB::query("DELETE FROM schuelerfach WHERE pid='$studpid' AND ref=2");
		DB::query('DELETE FROM darf WHERE vrid IN ('.implode(',',$vrids).')');
		DB::query('DELETE FROM voraus WHERE vrid IN ('.implode(',',$vrids).')');
		DB::query('DELETE FROM vrgraph WHERE vrid_vor IN ('.implode(',',$vrids).')');
		DB::query("DELETE FROM vorgang WHERE vgid=$vgid");
		header('Location: desktop.php');
		exit();
	}
	
	if (isset($_GET['resend']) && $u->gid==1) {
		include 'view.inc.php';
		include 'mail.inc.php';
		include 'tools.inc.php';
		$vgid=DB::sqli()->escape_string($_GET['resend']);
		$vg=new Vorgang($vgid,$u);
		$stu=new User($vg->get_stud_pid());
		$pwd=Tools::genpwd();
		DB::query('UPDATE person SET pass=\''.hash('sha256',$pwd).'\' WHERE pid=\''.$stu->pid.'\'');
		Tools::log('rpwd',"resend pw to " . $stu->mail);
		Mail::send ($stu,'Pruefungsantrag Humboldt-Oberschule','Fuer Sie liegt ein Antrag '.
			'zur Pruefung in der fuenften Pruefungskomponente vor.'."\n".
			'Bitte melden Sie sich unter http://pruefung.kurswahlonline.de an '.
			'und bearbeiten Sie Ihren Pruefungsantrag.'."\n\n".
			'Ihr Benutzername lautet: '.$stu->pid."\n".'Ihr Passwort lautet: '.$pwd.
			"\n".'Leider koennen Sie in der BETA-Phase das Passwort noch nicht selbst aendern.');
		header('Location: desktop.php');
		exit();
	}
	
	if (isset($_POST['chsub'])) {
		include 'view.inc.php';
		include 'tools.inc.php';
		$sub=$_POST['chsub'];
		$check=DB::get_value("SELECT COUNT(*) FROM fach WHERE fid='$sub'");
		if ($check!=1)
			die ('parameter error 0xAC');
		
		$vgid=$_POST['vgid'];
		if ($_POST['ptype']=='bll')
			$ptype='BLL';
		else
			$ptype='PRS';
		$vr=Vorgang::safe_create($vgid,$u);
		$spid=$vr->get_stud_pid();
		DB::query("UPDATE schuelerfach SET fid='$sub',type='$ptype' WHERE pid='$spid' AND ref=1");
		$flpid=Tools::get_fl_pid($sub);
		$vrids=implode(',',$vr->vrids);
		DB::query("UPDATE darf SET pid='$flpid' WHERE vrid IN ($vrids) AND role='flr'");
		header('Location: vorgang.php?vgid='.$vgid);
	}
	
?>