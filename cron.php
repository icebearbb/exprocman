<?php

include 'dbinterface.inc.php';
include 'mail.inc.php';

DB::connect();

// delete old mail entries (60 days)
DB::query('DELETE FROM cronqueue WHERE DATEDIFF(NOW(),wann)>60 AND ok=1 AND job=\'mail\'');

// delete old notification entries (2 days)
DB::query('DELETE FROM cronqueue WHERE DATEDIFF(NOW(),wann)>2 AND ok=1 AND job=\'noti\'');

// send mails
$mails=DB::get_assoc('SELECT an,betreff,body,name,jid FROM cronqueue WHERE ok=0 LIMIT 20');
foreach ($mails as $m) {
	Mail::sendcron($m['an'],$m['name'],$m['betreff'],$m['body']);
	$jid=$m['jid'];
	if (Mail::$lasterror!='') {
		DB::query("UPDATE cronqueue SET fehler='$lasterror' WHERE jid=$jid");
	} else {
		DB::query("UPDATE cronqueue SET ok=1 WHERE jid=$jid");
	}
}

// generate notifications, 2-day digest


// send notifications

?>