<?php
	include 'dbinterface.inc.php';
	include 'usermgr.inc.php';
	session_start();
	
	DB::connect();
	$u=User::auth_ok();
	if (!isset($u)) {
		header('Location: logout.php');
		exit;
	}
	if (isset($_POST['vrid'])) {
		$vrid=$_POST['vrid'];
		$sel=$_POST['sel'];
	} else
		if (isset($_GET['vrid'])) {
			$vrid=$_GET['vrid'];
			$sel='toggle';
		}
	if (!isset($vrid)) {
		header('Location: desktop.php');
		exit;
	}
	
	include 'view.inc.php';
	include 'tools.inc.php';

	$vr=new Voraussetzung($vrid,$u);
	
	if ($u->gid==2)
		die ('access denied');
	
	Tools::change_status($vr,$u,$sel);
	
	if ($vr->type=='accept' && $sel=='reject') {
		$spid=$vr->get_role_pid('stu');
		$sdata=DB::get_assoc("SELECT mail,name,vorname FROM person WHERE pid='$spid'");
		include 'mail.inc.php';
		Mail::sendm($sdata[0]['mail'],$sdata[0]['vorname'].' '.$sdata[0]['name'],
			'Pruefungskonzept abgelehnt','Liebe/r '.$sdata[0]['vorname'].' '.$sdata[0]['name']."!\n\n".
			'Leider wurde Ihr eingestelltes Pruefungskonzept abgelehnt. Bitte setzen Sie sich erneut '.
			'mit Ihrem Pruefer in Verbindung und klaeren Sie den Grund fuer die Ablehnung. Loggen Sie '.
			'sich dann in die Pruefungsplattform ein und ergaenzen bzw. korrigieren Sie bitte Ihr Konzept.'."\n\n".
			'Viele Gruesse - Humboldt-Pruefungsmanager');
	}
	
	header('Location: vorgang.php?vgid='.$vr->vgid);
	
?>