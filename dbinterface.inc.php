<?php

include 'config.db.php';

	class DB {
		
		private static $sqli=null;
		
		static function connect() {
			DB::$sqli= new mysqli(DBConfig::$host,DBConfig::$user,DBConfig::$pwd,DBConfig::$database);
			DB::$sqli->set_charset('utf8');
			return DB::$sqli;
		}
		
		static function get_user_val($pid,$attr) {
			if (!isset(DB::$sqli)) die ("DB::get_user_val ($query): no sqli");
			DB::log('DBIF get_user_val',$query);
			DB::test($pid);
			DB::test($attr);
			$res=DB::$sqli->query("SELECT $attr FROM person WHERE pid='$pid'");
			if (!$res) die (DB::$sqli->error);
			if ($row = $res->fetch_assoc()) {
				$res->free();
				return $row[$attr];
			} else {
				return false;
				die ("get_user_val ('$pid','$attr'): ".DB::$sqli->error);
			}
		}
		
		static function get_value($query) {
			if (!isset(DB::$sqli)) die ("DB::get_value ($query): no sqli");
			DB::log('DBIF get_value',$query);
			$res=DB::$sqli->query($query);
			if (!$res) die ("get_value ($query): ".DB::$sqli->error);
			if ($row = $res->fetch_row()) {
				$res->free();
				return $row[0];
			} else {
				die ("get_value ($query): ".DB::$sqli->error);
			}
		}
		
		static function get_list($query) {
			if (!isset(DB::$sqli)) die ("DB::get_list ($query): no sqli");
			$res=DB::$sqli->query($query);
			if (!$res) die ("get_list ($query): ".DB::$sqli->error);
			$data=array();
			while ($row = $res->fetch_row()) {
				$data[]=$row[0];
			}
			$res->free();
			return $data;
		}
		
		static function get_assoc($query) {
			if (!isset(DB::$sqli)) die ("DB::get_assoc ($query): no sqli");
			DB::log('DBIF get_assoc',$query);
			$res=DB::$sqli->query($query);
			if (!$res) die ("get_assoc ($query): ".DB::$sqli->error);
			$data=array();
			while ($row = $res->fetch_assoc()) {
				$data[]=$row;
			}
			$res->free();
			return $data;
		}
		
		static function get_assoc_row($query) {
			if (!isset(DB::$sqli)) die ("DB::get_assoc_row ($query): no sqli");
			DB::log('DBIF get_assoc_row',$query);
			$res=DB::$sqli->query($query);
			if (!$res) die ("get_assoc ($query): ".DB::$sqli->error);
			if ($row = $res->fetch_assoc()) {
				$res->free();
				return $row;
			} else {
				die ("get_assoc_row ($query): ".DB::$sqli->error);
			}
		}
		
		static function query($query) {
			DB::log('DBIF query',$query);
			if (!isset(DB::$sqli)) die ("DB::query ($query): no sqli");
			DB::$sqli->query($query);
			if (DB::$sqli->error!='') die ("DB::query ($query): ".DB::$sqli->error);
		}
		
		static function sqli() {
			return DB::$sqli;
		}
		
		static function esc($str) {
			return DB::$sqli->real_escape_string($str);
		}
		
		static function test($data) {
			if (DB::esc($data)!=$data) die ("input error");
		}
		
		static function log($tag,$data) {
			$fp = fopen('db.log', 'a');
			fwrite($fp, date("Y-m-d H:i:s").': '.$tag.': '.$data."\n");
			fclose($fp);
		}
		
	}
	
?>
