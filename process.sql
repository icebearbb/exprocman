-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 20. Apr 2021 um 18:25
-- Server-Version: 10.4.18-MariaDB
-- PHP-Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `process`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `angesehen`
--

CREATE TABLE `angesehen` (
  `pid` varchar(10) NOT NULL,
  `wann` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `darf`
--

CREATE TABLE `darf` (
  `pid` varchar(256) NOT NULL,
  `vrid` int(11) NOT NULL,
  `role` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `empfaengt`
--

CREATE TABLE `empfaengt` (
  `pid` int(11) NOT NULL,
  `nid` int(11) NOT NULL,
  `gelesen` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `fach`
--

CREATE TABLE `fach` (
  `fid` varchar(5) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `fach`
--

INSERT INTO `fach` (`fid`, `name`) VALUES
('EN', 'Englisch'),
('GE', 'Geschichte'),
('IN', 'Informatik');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `hatfach`
--

CREATE TABLE `hatfach` (
  `fid` varchar(5) NOT NULL,
  `pid` varchar(256) NOT NULL,
  `fl` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nachricht`
--

CREATE TABLE `nachricht` (
  `nid` int(11) NOT NULL,
  `nachr` text NOT NULL,
  `vgid` int(11) NOT NULL,
  `wann` datetime NOT NULL,
  `vonpid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `person`
--

CREATE TABLE `person` (
  `pid` varchar(256) NOT NULL,
  `pass` varchar(40) NOT NULL,
  `gid` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `vorname` varchar(100) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `gebdat` date NOT NULL,
  `ping` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `person`
--

INSERT INTO `person` (`pid`, `pass`, `gid`, `name`, `vorname`, `mail`, `gebdat`, `ping`) VALUES
('12345', 'dd7b7b74ea160e049dd128478e074ce47254bde8', 2, 'Pompööser', 'Peter', 'pete@pan.de', '2000-01-01', '2015-06-05 09:07:36'),
('54321', 'dd7b7b74ea160e049dd128478e074ce47254bde8', 2, 'Lolloger', 'Gerd', 'lolgerd@lo.de', '2001-01-01', '2015-06-05 06:51:20'),
('ADMIN', 'dd7b7b74ea160e049dd128478e074ce47254bde8', 1, 'Administrator', '', 'lol@lol.de', '0000-00-00', '2015-06-05 09:05:21'),
('PELZ', 'dd7b7b74ea160e049dd128478e074ce47254bde8', 3, 'Pelz', 'Lars', 'ib2000@gmx.de', '1979-03-14', '2015-06-05 09:05:40');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `schuelerfach`
--

CREATE TABLE `schuelerfach` (
  `pid` varchar(256) NOT NULL,
  `fid` varchar(5) NOT NULL,
  `ref` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `schuelerfach`
--

INSERT INTO `schuelerfach` (`pid`, `fid`, `ref`) VALUES
('54321', 'IN', 1),
('12345', 'EN', 1),
('54321', 'EN', 2),
('12345', 'EN', 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `voraus`
--

CREATE TABLE `voraus` (
  `vrid` int(11) NOT NULL,
  `inhalt` text NOT NULL,
  `typ` varchar(20) NOT NULL,
  `deadline` datetime NOT NULL,
  `status` varchar(5) NOT NULL,
  `vgid` int(11) NOT NULL,
  `letzt` datetime NOT NULL,
  `name` varchar(50) NOT NULL,
  `schalter` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `vorgang`
--

CREATE TABLE `vorgang` (
  `vgid` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `erstellt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `vrgraph`
--

CREATE TABLE `vrgraph` (
  `vrid_vor` int(11) NOT NULL,
  `vrid_nach` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `fach`
--
ALTER TABLE `fach`
  ADD PRIMARY KEY (`fid`);

--
-- Indizes für die Tabelle `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`pid`);

--
-- Indizes für die Tabelle `voraus`
--
ALTER TABLE `voraus`
  ADD PRIMARY KEY (`vrid`);

--
-- Indizes für die Tabelle `vorgang`
--
ALTER TABLE `vorgang`
  ADD PRIMARY KEY (`vgid`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `voraus`
--
ALTER TABLE `voraus`
  MODIFY `vrid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `vorgang`
--
ALTER TABLE `vorgang`
  MODIFY `vgid` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
