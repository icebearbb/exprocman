<?php

	include 'dbinterface.inc.php';
	include 'usermgr.inc.php';
	session_start();
	
	DB::connect();
	$u=User::auth_ok();
	if (!isset($u)) {
		header('Location: logout.php');
		exit;
	}
	
	include 'header.inc.php';
	echo Header::generate ($u);
	include 'menu.inc.php';
	echo Menu::generate ($u,'vorgang');
	include 'view.inc.php';
	if (isset($_GET['vgid'])) {
		$vg=Vorgang::safe_create($_GET['vgid'],$u);
	} else
		die ("parameter error 0x8F");
	
	if (isset($_GET['error'])) 
		switch ($_GET['error']){
			case 'mail':
				echo '<b style="color:red;">Bitte Mailadresse erneut eingeben!</b><br>';
				break;
			case 'text':
				echo '<b style="color:red;">Mind. eine Textangabe ist zu kurz oder fehlt.</b><br>';
				break;
		}
	
	$vrs=array();
	foreach ($vg->vrids as $v) {
		$vrs[]=new Voraussetzung($v,$u);
	}
	echo '<div style="text-align : left ;margin-left : 10% ;margin-right : 10% ;">';
	foreach ($vrs as $vr) {
		echo $vr->html().'<br>';
	}
	echo '</div><br><br><br><br>';
	
	include 'footer.inc.php';
	echo Footer::generate ($u);

?>