<?php

class VorgangListView extends View {
	
	public function html() {
		if (isset($this->u) && $this->u->gid!=1) {
			$pid="AND pid='".$this->u->pid."'";
		} else {
			$pid='';
		}
		$vgids=DB::get_list("SELECT DISTINCT vg.vgid FROM darf d,voraus v,vorgang vg 
				WHERE d.vrid=v.vrid AND v.vgid=vg.vgid $pid");
		$vgs=array();
		foreach ($vgids as $v) {
			$vgs[]=new Vorgang($v,$this->u);
		}
		// TODO: sort Vorgänge by user names
		$res='<div style="text-align:left;margin-left:10%;margin-right:10%;">'.
			'<table class="desktop" style="width=100%;border-collapse:collapse;"><tr>'.
			'<th>Vorgang</th><th>Prüfling</th><th>Prüfer</th>'.
			'<th>RF, FL</th><th>BF, FL</th><th>SL</th>';
		if ($this->u->gid==1) 
			$res.='<th class="buttons">Löschen</th>';
		$res.='</tr>';
		if (count ($vgs)!=0) {
			foreach ($vgs as $v) {
				$res.=$v->html();
			}
			$res.='</table></div>';
			return $res;
		} else {
			echo 'Es liegen keine Vorgänge für Sie vor.';
		}
	}
	
}

class View {
	
	protected $u=NULL;

	public function __construct($usr) {
		$this->u=$usr;
	}
	
	public function html() {
		
	}
	
	static function st_color($st) {
		switch ($st) {
			case 'open': return 'FF9966';
			case 'lock': return 'CCCCCC';
			case 'ok': return '99FF99';
			case 'clsd': return '99CCFF';
			case 'grp': return '77CCFF';
			default: return 'FFFF66';
		}
	}
	
}

class Vorgang extends View {
	
	public $vgid=0;
	public $pids=array();  
	public $vrids=array();
	public $tasks=array(); // keys: pid, gid, vrid, role, status, type
	public $name='noname';
	public $grpids=array();
	public $export=0;
	public $extype='no';

	public function __construct($vgid,$usr) {
		$this->u=$usr;
		$this->vgid=DB::sqli()->escape_string($vgid);
		$tmp=DB::get_assoc("SELECT name,export FROM vorgang WHERE vgid=$vgid");
		$this->name=$tmp[0]['name'];
		$this->export=$tmp[0]['export'];
		$this->tasks=DB::get_assoc("SELECT d.pid,d.vrid,d.role,v.status,v.typ FROM darf d,person p,voraus v
			WHERE p.pid=d.pid AND d.vrid=v.vrid AND v.vgid=$vgid ORDER BY d.vrid");
		foreach ($this->tasks as $t) {
			if (!in_array($t['vrid'],$this->vrids)) $this->vrids[]=$t['vrid'];
			if (!in_array($t['pid'],$this->pids)) $this->pids[]=$t['pid'];
		}
		// group exam
		$grp=DB::get_assoc("SELECT vgid1,vgid2 FROM gpruef WHERE vgid1=$vgid OR vgid2=$vgid");
		foreach ($grp as $g) {
			if (!in_array($g['vgid1'],$this->grpids) && $g['vgid2']==$vgid)
				$this->grpids[]=$g['vgid1'];
			if (!in_array($g['vgid2'],$this->grpids) && $g['vgid1']==$vgid)
				$this->grpids[]=$g['vgid2'];
		}
		// exam type
		$this->extype=DB::get_value("SELECT type FROM schuelerfach WHERE pid='".$this->get_stud_pid()."' AND ref=1");
	}
	
	public function html() {
		$usr=array();
		$res="";
		$stu_ok=0;
		$subb='--';
		foreach ($this->tasks as $t) {
			include 'tools.inc.php';
			Tools::log('Vorgang::html',$t['pid'].'->'.$t['role']);
			switch ($t['role']) {
				case 'stu':
					if (!isset($stu)) {
						$stu=new User($t['pid'],2);
						$spid=$t['pid'];
					}
					break;
				case 'exm':
					if (!isset($exm)) $exm=new User($t['pid'],3);
					break;
				case 'flr':
					if (!isset($flr)) $flr=new User($t['pid'],3);
					break;
				case 'flb':
					if (!isset($flb)) $flb=new User($t['pid'],3);
					break;
				case 'sl':
					if (!isset($sl)) $sl=new User($t['pid'],4);
					break;
			}
			switch ($t['typ']) {
				case 'con':
				case 'info':
				case 'mail':
				case 'bfach':
					if ($t['role']=='stu' && $t['status']=='ok') $stu_ok++;
					break;
				default:
					$st_col[$t['typ']]=View::st_color($t['status']);
			}
		}
		if (!isset($stu)) {
			// should never happen
			echo ('no student pid for vgid '.$this->vgid);
			return;
		}
		$subd=DB::get_assoc("SELECT fid,ref FROM schuelerfach WHERE pid='$spid'");
		foreach ($subd as $s) {
			if ($s['ref']==1) $subr=$s['fid'];
			if ($s['ref']==2) $subb=$s['fid'];
		}
		
		if ($stu_ok==3) $st_col['stu']=View::st_color('ok'); else $st_col['stu']=View::st_color('open');
		$res.='<tr><td'.(count($this->grpids)>0?' bgcolor="'.View::st_color('grp').'"':'').
			'><a href="vorgang.php?vgid='.$this->vgid.'">'.
			($this->extype=='BLL'?'<b style="color:darkgreen;">BLL</b>':$this->extype).'</a></td>'.
			'<td bgcolor="'.$st_col['stu'].'">'.$stu->vorname.' '.$stu->name.'</td>'.
			'<td bgcolor="'.$st_col['accept'].'">'.$exm->vorname.' '.$exm->name.'</td>'.
			'<td bgcolor="'.$st_col['flref'].'">'.'<b>'.$subr.'</b>: '.$flr->vorname.' '.$flr->name.'</td>'.
			'<td bgcolor="'.$st_col['flbez'].'">'.(isset($flb)?'<b>'.$subb.'</b>: '.$flb->vorname.' '.$flb->name:'n/a').'</td>'.
			'<td bgcolor="'.$st_col['sl'].'">'.$sl->vorname.' '.$sl->name.'</td>';
		if ($this->u->gid==1 || !isset($this->u))
			$res.='<td class="buttons"><button onclick="document.location=\'procman.php?delvg='.$this->vgid.
				'\';">Löschen</button></td>';
		$res.="</tr>\n";
		return $res;
	}
	
	static function safe_create($vgid, $u) {
		if (is_numeric($vgid)) 
			$vg=new Vorgang($vgid,$u);
		else
			die ("parameter error 0x5F");
		if ($u->gid=='1') return $vg;
		foreach ($vg->pids as $p) {
			if ($p==$u->pid) return $vg;
		}
		//Tools::log('vg_safe_create','vgid '.$vgid.': pid '.$u->pid.' not in pids');
		return $vg;
		//die ("not authorized 0x03");
	}
	
	public function get_stud_pid() {
		foreach ($this->tasks as $t) {
			if ($t['role']=='stu') return $t['pid'];
		}
	}
	
	public function get_stud_name() {
		$stu=$this->get_stud_pid();
		$row=DB::get_assoc_row("SELECT name,vorname FROM person WHERE pid='$stu'");
		return $row['vorname'].' '.$row['name'];
	}
	
}

class Voraussetzung extends View {
	public $pids=array();
	public $vrid=-1;
	public $data='no';
	public $type='no';
	public $status='open';
	public $lastchg='';
	public $name='no';
	public $vgid=-1;
	public $deadline='';
	public $trigger='';
	public $texts=array();
	
	public function __construct($vrid,$usr) {
		if ($vrid==-1) return;
		if (isset($usr)) $this->u=$usr;
		$this->vrid=$vrid;
		$info=DB::get_assoc_row("SELECT * FROM voraus WHERE vrid=$vrid");
		$this->data=$info['inhalt'];
		$this->type=$info['typ'];
		$this->name=$info['name'];
		$this->status=$info['status'];
		$this->lastchg=$info['letzt'];
		$this->vgid=$info['vgid'];
		$this->deadline=$info['deadline'];
		$this->trigger=$info['schalter'];
		$this->pids=DB::get_assoc("SELECT pid,role FROM darf WHERE vrid=$vrid");
		$this->texts=DB::get_assoc("SELECT vkey,vdata FROM vrdata WHERE vrid=$this->vrid");
		//include 'tools.inc.php';
		$keys=array();
		if ($this->type=='con') {
			$keys=array('motivation','material','intersub','abstract');
		}
		if ($this->type=='info') {
			$keys=array('topic','quest');
		}
		$this->texts=Voraussetzung::transpose($keys,$this->texts);
	}
	
	public static function transpose($keys,$assoc) {
		$res=array();
		foreach ($keys as $k) {
			foreach ($assoc as $row) {
				if ($row['vkey']==$k) $res[$k]=$row['vdata'];
			}
		}
		return $res;
	}
	
	public static function create() {
		return new Voraussetzung(-1,NULL);
	}
	
	public static function get_status($vrid) {
		return DB::get_value("SELECT status FROM voraus WHERE vrid=$vrid");
	}
	
	public function get_role_pid($role) {
		foreach ($this->pids as $p) 
			if ($p['role']==$role)
				return $p['pid'];
	}
	
	public function get_role_user($usr,$role) {
		foreach ($this->pids as $p) {
			if ($p['role']==$role)
				foreach ($usr as $u) {
					if ($u->pid==$p['pid'])
						return $u;
				}
		}
	}
	
	public function html() {
		include 'tools.inc.php';
		$bg=View::st_color($this->status);
		$res='<div style="background-color:#'.$bg.'">
			Voraussetzung: <b>'.$this->name.'</b>, ';
		
		$usr=array();
		include 'tools.inc.php';
		foreach ($this->pids as $pid) {
			Tools::log('Voraussetzung::html',$pid['pid']);
			if (strpos($pid['pid'],'.')>2) $gid=2; else $gid=3;
			$usr[]=new User($pid['pid'],$gid);
		}
		
		switch ($this->type) {
			case 'info':
			case 'mail':
			case 'con':
			case 'bfach':
			case 'doc':
				$resp=$this->get_role_user($usr,'stu');
				break;
			case 'accept':
				$resp=$this->get_role_user($usr,'exm');
				break;
			case 'flref':
				$resp=$this->get_role_user($usr,'flr');
				break;
			case 'flbez':
				$resp=$this->get_role_user($usr,'flb');
				break;
			case 'sl':
				$resp=$this->get_role_user($usr,'sl');
				break;
			default:
				$resp=(object)array('vorname' => 'ERR','name'=>'');
		}
		if (!isset($resp)) $resp=(object)array('vorname' => 'N/A','name'=>'');
		$res.='verantwortlich: <b>'.$resp->vorname.' '.$resp->name.'</b><br>';
		
		if ($this->status!='lock') {
		switch ($this->type) {
		case 'mail':
			if (($this->get_role($this->u->pid)=='stu' && ($this->status=='open')) || $this->u->gid==1 ){
				$res.='<form action="datastore.php" method="POST">
					Mail-Adresse:  <input type="text" name="mail" value="'.
					$this->get_role_user($usr,'stu')->mail.'">'.
					' Wiederholung:  <input type="text" name="mailrep" value="'.
					$this->get_role_user($usr,'stu')->mail.'">'.
					$this->hid_vrid().'
					<input type="submit" value="Eintragen">
					<input type="hidden" name="mode" value="mail">
					</form>';
			} else {
				$res.='Mail-Adresse: '.$this->get_role_user($usr,'stu')->mail;	
			}
			break;
		case 'info':
			if ( ($this->get_role($this->u->pid)=='stu' || $this->u->gid==1)
					&& ($this->status=='open') ) {
				$res.='<form action="datastore.php" method="POST"><table><tr>
					<td>Thema:</td><td><input type="text" name="topic" value="'.
					(isset($this->texts['topic'])?$this->texts['topic']:'').
					'" size="80"></td></tr><tr>
					<td>Leitfrage:</td><td><input type="text" name="quest" value="'.
					(isset($this->texts['quest'])?$this->texts['quest']:'').
					'" size="80"></td></tr></table>'
					.$this->hid_vrid().'
					<input type="submit" value="Eintragen">
					<input type="hidden" name="mode" value="info">
					</form>
					'."\n";
			} else {
				$res.='<table class="desktop"><tr><td>Thema:</td><td>'.
				Tools::xssprotect((isset($this->texts['topic'])?$this->texts['topic']:'MIF'))
				.'</td></tr><tr><td>Leitfrage:</td><td>'.
				Tools::xssprotect((isset($this->texts['quest'])?$this->texts['quest']:'MAF')).'</td></tr></table>';
			}
			break;
		case 'con':
			if ( ($this->get_role($this->u->pid)=='stu' || $this->u->gid==1)
					&& ($this->status=='open') ){
				$res.='<form action="datastore.php" method="POST"><table><tr><td>Interesse am Thema</td>'.
					'<td><textarea rows="4" cols="30" name="motivation">'.
					(isset($this->texts['motivation'])?$this->texts['motivation']:'').
					'</textarea></td><td>Beschreiben Sie Ihre Motivation, sich mit dem Thema auseinandersetzen zu wollen, ggf. persönliche Erlebnisse, Film, Fernsehen, Zeitgeschehen o.ä.</td></tr>'.
					'<tr><td>Material</td>'.
					'<td><textarea rows="4" cols="30" name="material">'.
					(isset($this->texts['material'])?$this->texts['material']:'').
					'</textarea></td>
					<td>Nennen Sie welche wesentlichen Grundlagen (welche Literatur, welches Experiment, welche Befragung) Sie für Ihre Arbeit verwenden wollen. Woher stammen die Informationen, die Sie verarbeiten wollen?</td></tr>'.
					'<tr><td>fach&uuml;bergreifender Aspekt</td>'.
					'<td><textarea rows="4" cols="30" name="intersub">'.
					(isset($this->texts['intersub'])?$this->texts['intersub']:'').
					'</textarea></td> <td>Erläutern Sie inwiefern das Bezugsfach in der Bearbeitung und im Vortrag eine Rolle spielt.</td></tr>'.
					'<tr><td>Abstract</td>'.
					'<td><textarea rows="4" cols="30" name="abstract">'.
					(isset($this->texts['abstract'])?$this->texts['abstract']:'').
					'</textarea></td><td>Skizzieren Sie in einigen Sätzen den groben inhaltlichen
					Aufbau Ihrer Arbeit (Abstract).</td></tr></table>'.
					$this->hid_vrid().
					'<input type="submit" value="Eintragen">
					<input type="hidden" name="mode" value="con"></form>';
			} else {
				$res.='<table class="desktop"><tr><td>Interesse am Thema</td>'.
					'<td>'.Tools::xssprotect((isset($this->texts['motivation'])?$this->texts['motivation']:'')).'</td></tr>'.
					'<tr><td>Material</td>'.
					'<td>'.Tools::xssprotect((isset($this->texts['material'])?$this->texts['material']:'')).'</td></tr>'.
					'<tr><td>fach&uuml;bergreifender Aspekt</td>'.
					'<td>'.Tools::xssprotect((isset($this->texts['intersub'])?$this->texts['intersub']:'')).'</td></tr>'.
					'<tr><td>Abstract</td>'.
					'<td>'.Tools::xssprotect((isset($this->texts['abstract'])?$this->texts['abstract']:'')).'</td></tr></table>';
			}
			break;
		case 'bfach':
			$res.='Referenzfach: <b>'.$this->get_role_user($usr,'stu')->rfach.'</b>'.
			', Bezugsfach: <b>'.$this->get_role_user($usr,'stu')->bfach.'</b>';
			if ( ($this->get_role($this->u->pid)=='stu' || $this->u->gid==1)
					&& ($this->status=='open') ){
				$subj=DB::get_assoc('SELECT fid,name FROM fach WHERE NOT fid=\''.$this->u->rfach.'\'');
				$res.='<form action="datastore.php" method="POST">
					Bitte Bezugsfach auswählen: <select name="subj">';
				foreach ($subj as $s) {
					$res.='<option value="'.$s['fid'].'">'.$s['name'].'</option>';
				}
				$res.='</select>
					<input type="hidden" name="mode" value="bfach">
					<input type="submit" value="Eintragen">
				'.$this->hid_vrid().'
				</form>';
				if ($this->u->gid==1) {
					$res.='<form action="procman.php" method="POST">
						<input type="hidden" name="vgid" value="'.$this->vgid.'">
						Ref.-Fach wählen: <select name="chsub">';
					$spid=$this->get_role_pid('stu');
					$fdata=DB::get_assoc_row("SELECT fid,type FROM schuelerfach WHERE pid='$spid' and ref=1");
					$rfach=$fdata['fid'];
					$ptype=$fdata['type'];
					$subjs=DB::get_list("SELECT fid FROM fach");
					foreach ($subjs as $s) {
						$res.='<option value="'.$s.'" ';
						if ($s==$rfach) $res.='selected';
						$res.='>'.$s.'</option>';
					}
					$res.='</select> Prüfungsart wählen: <select name="ptype">
						<option value="bll">BLL</option>
						<option value="prs" '.($ptype=='PRS'?'selected':'').'>PRS</option>
						</select>
						<input type="submit" value="Ändern">
						</form>';
				}
			} 
			break;
		case 'accept':
			if ($this->get_role($this->u->pid)=='exm' || $this->u->gid==1) $res.=$this->approve_form();
			break;
		case 'flref':
			if ($this->has_role($this->u->pid,'flr') || $this->u->gid==1) $res.=$this->approve_form();
			break;
		case 'flbez':
			if ($this->has_role($this->u->pid,'flb') || $this->u->gid==1) $res.=$this->approve_form();
			break;
		case 'sl':
			if ($this->u->gid==4 || $this->u->gid==1) $res.=$this->approve_form();
			break;
		}
		}
		if ($this->u->gid==1 && $this->status=='ok') $res.=$this->gen_reset_link($this);
		$res.='</div>';
		return $res;
	}
	
	public function approve_form() {
		if ($this->status!='open') return;
		$res='<form name="appr" action="approve.php" method="POST">';
			$res.='<input type="button" value="Zustimmen" onclick="
				document.forms[\'appr\'].sel.value=\'ok\';
				document.forms[\'appr\'].submit();
				">';
			$res.='<input type="button" value="Ablehnen" onclick="
				document.forms[\'appr\'].sel.value=\'reject\';
				document.forms[\'appr\'].submit();
				">';
			$res.=$this->hid_vrid().
				'<input type="hidden" name="sel"></form>';
		return $res;
	}
	
	public function hid_vrid() {
		return '<input type="hidden" name="vrid" value="'.$this->vrid.'">';
	}
	
	public function update_status($st) {
		$this->status=$st;
		DB::query("UPDATE voraus SET status='$st' WHERE vrid='$this->vrid'");
	}
	
	public function get_role($pid) {
		foreach ($this->pids as $p) {
			if ($p['pid']==$pid) return $p['role'];
		}
	}
	
	public function has_role($pid,$r) {
		foreach ($this->pids as $p) {
			if ($p['pid']==$pid && $p['role']==$r) return true;
		}
		return false;
	}
	
	public function commit() {
		if ($this->vrid==-1) {
			$tmp=DB::get_value("SELECT MAX(vrid) FROM voraus");
			$this->vrid=$tmp+1;
			switch ($this->type) {
				case 'con':
				case 'info':
				DB::query("INSERT INTO voraus (vrid,vgid,typ,deadline,status,letzt,name) VALUES ($this->vrid,$this->vgid,'$this->type','$this->deadline','$this->status',NOW(),'$this->name')");
				DB::query("DELETE FROM vrdata WHERE vrid=$this->vrid");
				$keys=array_keys($this->texts);
				Tools::log('vr_commit',count($keys));
				foreach ($keys as $k) {
					$v=DB::esc($this->texts[$k]);
					DB::query("INSERT INTO vrdata (vrid,vkey,vdata) VALUES ($this->vrid,'$k','$v')");
				}
				break;
				default:
				DB::query("INSERT INTO voraus (vrid,vgid,inhalt,typ,deadline,status,letzt,name)
				VALUES ($this->vrid,$this->vgid,'$this->data','$this->type','$this->deadline','$this->status',NOW(),'$this->name')");
			}
			DB::query("DELETE FROM darf WHERE vrid=$this->vrid");
			foreach ($this->pids as $pid) {
				$p=$pid['pid'];
				$r=$pid['role'];
				DB::query("INSERT INTO darf (vrid,pid,role) VALUES ($this->vrid,'$p','$r')");
			}
		} else {
			//$this->data=preg_replace("/'/",'**',$this->data);
			switch ($this->type) {
				case 'con':
				case 'info':
				DB::query("UPDATE voraus SET typ='$this->type',deadline='$this->deadline',status='$this->status',letzt=NOW(),name='$this->name' WHERE vrid=$this->vrid");
				DB::query("DELETE FROM vrdata WHERE vrid=$this->vrid");
				$keys=array_keys($this->texts);
				Tools::log('vr_commit',count($keys));
				foreach ($keys as $k) {
					$v=DB::esc($this->texts[$k]);
					DB::query("INSERT INTO vrdata (vrid,vkey,vdata) VALUES ($this->vrid,'$k','$v')");
				}
				break;
				default:
				DB::query("UPDATE voraus SET inhalt='$this->data',typ='$this->type',deadline='$this->deadline',status='$this->status',letzt=NOW(),name='$this->name' WHERE vrid=$this->vrid");
			}
		}
	}
	
	private function gen_reset_link($vr) {
		return '<br><button onclick="document.location=\'reset.php?reset='.$vr->vrid.'&mode='.$vr->type.'\';">Zurücksetzen</button>';
	}
	
	static function safe_create($vrid, $u) {
		if (is_numeric($vrid)) 
			$vr=new Voraussetzung($vrid,$u);
		else
			die ("parameter error 0xAF");
		if ($u->gid=='1') return $vr;
		foreach ($vr->pids as $p) {
			if ($p['pid']==$u->pid) return $vr;
		}
		die ("not authorized 0x04");
	}
	
	public function may_change($u) {
		if ($u->gid=='1') return true;
		if ($u->gid=='2' && in_array($this->type,array('con','info','mail','bfach'))) return true;
		if ($u->gid=='3' && in_array($this->type,array('accept','flref','flbez'))) return true;
		if ($u->gid=='4' && in_array($this->type,array('sl'))) return true;
		return false;
	}
	
}

?>
