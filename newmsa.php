<?php
	include 'dbinterface.inc.php';
	include 'usermgr.inc.php';
	session_start();
	
	DB::connect();
	$u=User::auth_ok();
	if (!isset($u)) {
		header('Location: logout.php');
		exit;
	}
	
	include 'header.inc.php';
	echo Header::generate_scripts ($u,array('newvg','axobj'));
	include 'menu.inc.php';
	echo Menu::generate ($u,'newvg');
	
	include 'view.inc.php';
	include 'tools.inc.php';
	
	if (isset($_POST['mode'])) $mode=$_POST['mode']; else $mode='';
	if (isset($_POST['pid'])) $studpid=DB::sqli()->escape_string($_POST['pid']); else $studpid='';
	if (isset($_POST['deadline'])) $dl=DB::sqli()->escape_string($_POST['deadline']); else $dl='';
	switch ($mode) {
		case 'create':
			// create Vorgang
			$vgid=DB::get_value("SELECT MAX(vgid) FROM vorgang");
			$vgid++;
			DB::query("INSERT INTO vorgang (name,vgid,erstellt) VALUES ('5PK',$vgid,NOW())");
			
			$stud=new User($studpid);
			$flpid=Tools::get_fl_pid($stud->rfach);
			
			// create multiple Voraussetzung
			$vmail=Voraussetzung::create();
			$vmail->name='Mail-Adresse';
			$vmail->pids[]=array('pid'=>$u->pid,'role'=>'exm');
			$vmail->pids[]=array('pid'=>$studpid,'role'=>'stu');
			$vmail->pids[]=array('pid'=>$flpid,'role'=>'flr');
			$vmail->type="mail";
			$vmail->deadline=$dl;
			$vmail->vgid=$vgid;
			$vmail->commit();
			
			$vref=Voraussetzung::create();
			$vref->name='Bezugsfach wählen';
			$vref->pids[]=array('pid'=>$u->pid,'role'=>'exm');
			$vref->pids[]=array('pid'=>$studpid,'role'=>'stu');
			$vref->pids[]=array('pid'=>$flpid,'role'=>'flr');
			$vref->type="bfach";
			$vref->deadline=$dl;
			$vref->vgid=$vgid;
			$vref->status='open';
			$vref->trigger='setfl';
			$vref->commit();
			
			$vinfo=Voraussetzung::create();
			$vinfo->name='Prüfungsinformationen angeben';
			$vinfo->pids[]=array('pid'=>$u->pid,'role'=>'exm');
			$vinfo->pids[]=array('pid'=>$studpid,'role'=>'stu');
			$vinfo->pids[]=array('pid'=>$flpid,'role'=>'flr');
			$vinfo->type="info";
			$vinfo->deadline=$dl;
			$vinfo->vgid=$vgid;
			$vinfo->status='lock';
			$vinfo->commit();
			
			$vcon=Voraussetzung::create();
			$vcon->name='Konzept erstellen';
			$vcon->pids[]=array('pid'=>$u->pid,'role'=>'exm');
			$vcon->pids[]=array('pid'=>$studpid,'role'=>'stu');
			$vcon->pids[]=array('pid'=>$flpid,'role'=>'flr');
			$vcon->type="con";
			$vcon->deadline=$dl;
			$vcon->vgid=$vgid;
			$vcon->status='lock';
			$vcon->commit();
			
			$vacc=Voraussetzung::create();
			$vacc->name='Annahme der Prüfung';
			$vacc->pids[]=array('pid'=>$u->pid,'role'=>'exm');
			$vacc->pids[]=array('pid'=>$studpid,'role'=>'stu');
			$vacc->pids[]=array('pid'=>$flpid,'role'=>'flr');
			$vacc->type="accept";
			$vacc->deadline=$dl;
			$vacc->vgid=$vgid;
			$vacc->status='lock';
			$vacc->commit();
			
			$vflref=Voraussetzung::create();
			$vflref->name='Fachleiter Referenzfach';
			$vflref->pids[]=array('pid'=>$u->pid,'role'=>'exm');
			$vflref->pids[]=array('pid'=>$studpid,'role'=>'stu');
			$vflref->pids[]=array('pid'=>$flpid,'role'=>'flr');
			$vflref->type="flref";
			$vflref->deadline=$dl;
			$vflref->vgid=$vgid;
			$vflref->status='lock';
			$vflref->trigger='fl';
			$vflref->commit();
			
			$vflbez=Voraussetzung::create();
			$vflbez->name='Fachleiter Bezugsfach';
			$vflbez->pids[]=array('pid'=>$u->pid,'role'=>'exm');
			$vflbez->pids[]=array('pid'=>$studpid,'role'=>'stu');
			$vflbez->pids[]=array('pid'=>$flpid,'role'=>'flr');
			$vflbez->type="flbez";
			$vflbez->deadline=$dl;
			$vflbez->vgid=$vgid;
			$vflbez->status='lock';
			$vflbez->trigger='fl';
			$vflbez->commit();
			
			$vsl=Voraussetzung::create();
			$vsl->name='Schulleiter';
			$vsl->pids[]=array('pid'=>$u->pid,'role'=>'exm');
			$vsl->pids[]=array('pid'=>$studpid,'role'=>'stu');
			$vsl->pids[]=array('pid'=>$flpid,'role'=>'flr');
			$vsl->pids[]=array('pid'=>Tools::get_sl_pid(),'role'=>'sl');
			$vsl->type="sl";
			$vsl->deadline=$dl;
			$vsl->vgid=$vgid;
			$vsl->status='lock';
			$vsl->trigger='';
			$vsl->commit();
			
			// create follow-up structure
			$vrg=new VRGraph();
			$vrg->set_vgid($vgid);
			$vrg->add_edge ($vmail->vrid,$vinfo->vrid);
			$vrg->add_edge ($vref->vrid,$vinfo->vrid);
			$vrg->add_edge ($vmail->vrid,$vcon->vrid);
			$vrg->add_edge ($vref->vrid,$vcon->vrid);
			$vrg->add_edge ($vinfo->vrid,$vacc->vrid);
			$vrg->add_edge ($vcon->vrid,$vacc->vrid);
			$vrg->add_edge ($vacc->vrid,$vflref->vrid);
			$vrg->add_edge ($vacc->vrid,$vflbez->vrid);
			$vrg->add_edge ($vflref->vrid,$vsl->vrid);
			$vrg->add_edge ($vflbez->vrid,$vsl->vrid);
			$vrg->commit();
			
			// generate and set testee password
			$pwd=Tools::genpwd();
			DB::query('UPDATE person SET pass=\''.hash('sha256',$pwd).'\' WHERE pid=\''.$studpid.'\'');
			
			$studusr=new User($studpid);
			
			// notify testee
			include 'mail.inc.php';
			Mail::send ($studusr,'Pruefungsantrag Humboldt-Oberschule','Sie wurden von '.
				$u->vorname.' '.$u->name.
				' zur Beantragung einer Pruefung in der fuenften Pruefungskomponente eingeladen.'."\n".
				'Bitte melden Sie sich unter http://pruefung.kurswahlonline.de an '.
				'und bearbeiten Sie Ihren Pruefungsantrag.'."\n\n".
				'Ihr Benutzername ist '.$studusr->pid.' und Ihr Passwort ist '.$pwd.
				'.'."\n".'Leider koennen Sie in der BETA-Phase das Passwort noch nicht selbst aendern.');
			
			header("Location: vorgang.php?vgid=$vgid");
		default:	
?>
Leider noch nicht möglich.
<!--
Neuen Vorgang (5PK) anlegen für Schüler:
<form method="POST" action="newvg.php">
<select name="pid">
<option name="no">--- Bitte wählen! ---</option>'

<?php
	$stud=DB::get_assoc('SELECT name,vorname,pid FROM person WHERE gid=2 AND pid NOT IN (SELECT pid FROM darf) ORDER BY name');
	
	foreach ($stud as $st) {
		echo '<option value="'.$st['pid'].'">'.
			$st['vorname'].' '.$st['name'].'</option>';
	}
?>
	</select><br>
	<input type="hidden" name="mode" value="create">
	<input type="submit" value="Anlegen">
	</form>
-->
<?php
	}
	include 'footer.inc.php';
	echo Footer::generate ($u);
?>