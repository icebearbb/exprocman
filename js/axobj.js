axobj=false;
saved=true;

if (typeof XMLHttpRequest != 'undefined') {
    axobj = new XMLHttpRequest();
}

if (!axobj) {
    try {
        axobj = new ActiveXObject("Msxml2.XMLHTTP");
    } catch(ex) {
        try {
            axobj = new ActiveXObject("Microsoft.XMLHTTP");
        } catch(ex) {
            axobj = null;
        }
    }
}

function axsend (data,cgi,handler) {

	axobj.open('POST',cgi);
	axobj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	axobj.setRequestHeader("Content-length", data.length);
	axobj.setRequestHeader("Connection", "close");
    axobj.onreadystatechange = handler;
    axobj.send(params);
	
	saved=true;
}