<?php

	include 'dbinterface.inc.php';
	include 'usermgr.inc.php';
	session_start();
	
	DB::connect();
	$u=User::auth_ok();
	if (!isset($u)) {
		header('Location: logout.php');
		exit;
	}
	include 'view.inc.php';
	include 'tools.inc.php';

	if (isset($_GET['reset'])) {
		$vrid=$_GET['reset'];
		$vr=Voraussetzung::safe_create($vrid,$u);
		if (isset($_GET['mode']))
			$mode=$_GET['mode'];
		else 
			$mode='';
	} else 
		die ('parameter error 0xAC');
	
	$vgid=$vr->vgid;
	
/*	switch ($mode) {
		case 'mail':
		case 'info':
		case 'con':
		case 'bfach':*/
			Tools::change_status($vr,$u,'open');
			header('Location: vorgang.php?vgid='.$vgid);
/*		break;
		default:
			echo 'parameter error 0xCC';
	}*/
	

?>