<?php

class Header {
	static $doctype='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
			"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
	static $headertags='<meta charset="utf-8">';
	static $styles='<link href="procstyle.css" type="text/css" rel="stylesheet" />';
	
	static function generate($u) {
		$res=Header::$doctype.
			'<html>
				<head>'.
					Header::$styles.
					Header::$headertags.
				'</head>
			<body>';
		return $res;
	}
	
	static function generate_scripts($u,$scripts) {
		$res=Header::$doctype.
			'<html>
				<head>'.
					Header::$styles.
					Header::$headertags;
		foreach ($scripts as $s) {
			$res.='<script type="text/javascript" src="js/'.$s.'.js"></script>';
		}
		$res.='</head>
			<body>';
		return $res;
	}
}

?>