<?php
	
	include 'dbinterface.inc.php';
	include 'usermgr.inc.php';
	session_start();
	
	DB::connect();
	$u=User::auth_ok();
	if (!isset($u)) {
		header('Location: logout.php');
		exit;
	}
	include 'view.inc.php';
	include 'tools.inc.php';
	
	function store_text ($vr,$u,$keys,$data,$vgid) {
		foreach ($keys as $k) {
			$vr->texts[$k]=$data[$k];
		}
		$vr->commit();
		if (!Tools::data_empty($keys,$data)) {
			Tools::change_status($vr,$u,'ok');
			header("Location: vorgang.php?vgid=$vgid");
		} else {
			header("Location: vorgang.php?vgid=$vgid&error=text");
		}
	}
	
	if (isset($_POST['vrid'])) {
		$vrid=$_POST['vrid'];
		$vr=Voraussetzung::safe_create($vrid,$u);
		if (isset($_POST['mode']))
			$mode=$_POST['mode'];
		else 
			$mode='';
	} else
		die ('parameter error 0xDD');
	
	$vgid=$vr->vgid;
	switch ($mode) {
		case 'mail':
			if (isset($_POST['mail'])) {
				$mail=DB::esc($_POST['mail']);
				$mailrep=DB::esc($_POST['mail']);
				if ($mail!=$mailrep){
					header("Location: vorgang.php?vgid=$vgid&error=mail");
					exit();
				}
				if ($vr->get_role($u->pid)!='stu')
					$pid=$vr->get_role_pid('stu');
				else
					$pid=$u->pid;
				if (Tools::check_email_address($mail)) {
					DB::query("UPDATE person SET mail='$mail' WHERE pid='$pid'");
					Tools::change_status($vr,$u,'ok');
					header("Location: vorgang.php?vgid=$vgid");
				} else {
					DB::query("UPDATE person SET mail='' WHERE pid='$pid'");
					Tools::change_status($vr,$u,'open');
					header("Location: vorgang.php?vgid=$vgid&error=mail");
				}
			}
			break;
		case 'bfach':
			if (isset($_POST['subj'])) {
				if ($vr->get_role($u->pid)!='stu')
					$pid=$vr->get_role_pid('stu');
				else
					$pid=$u->pid;
				$subj=DB::esc($_POST['subj']);
				DB::query ("DELETE FROM schuelerfach WHERE pid='$pid' AND ref=2");
				DB::query ("INSERT INTO schuelerfach (pid,fid,ref) VALUES ('$pid','$subj',2)");
				$flpid=Tools::get_fl_pid($subj);
				$vg=new Vorgang($vgid,$u);
				DB::query ('DELETE FROM darf WHERE vrid IN ('.implode(',',$vg->vrids).") AND role='flb'");
				foreach ($vg->vrids as $v) 
					DB::query ("INSERT INTO darf (pid,vrid,role) VALUES ('$flpid',$v,'flb')");
				Tools::change_status($vr,$u,'ok');
				header("Location: vorgang.php?vgid=$vgid");
			}
			break;
		case 'info':
			$keys=array('topic','quest');
			store_text ($vr,$u,$keys,$_POST,$vgid);
			break;
		case 'con':
			$keys=array('motivation','material','intersub','abstract');
			store_text ($vr,$u,$keys,$_POST,$vgid);
	}
	
?>