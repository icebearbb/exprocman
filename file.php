<?php
	include 'dbinterface.inc.php';
	include 'usermgr.inc.php';
	session_start();
	
	DB::connect();
	$u=User::auth_ok();
	if (!isset($u)) {
		header('Location: logout.php');
		exit;
	}
	
	include 'view.inc.php';
	if (isset($_POST['vrid'])) {
		$vr=new Voraussetzung($_POST['vrid'],$u);
	}

// taken from
// http://api.drupalcommerce.org/api/Drupal%20Commerce/includes!file.inc/function/file_upload_max_size/DC

// Returns a file size limit in bytes based on the PHP upload_max_filesize
// and post_max_size
function file_upload_max_size() {
  static $max_size = -1;

  if ($max_size < 0) {
    // Start with post_max_size.
    $max_size = parse_size(ini_get('post_max_size'));

    // If upload_max_size is less, then reduce. Except if upload_max_size is
    // zero, which indicates no limit.
    $upload_max = parse_size(ini_get('upload_max_filesize'));
    if ($upload_max > 0 && $upload_max < $max_size) {
      $max_size = $upload_max;
    }
  }
  return $max_size;
}

// taken from
// http://api.drupalcommerce.org/api/Drupal%20Commerce/includes!common.inc/function/parse_size/DC

function parse_size($size) {
  $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
  $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
  if ($unit) {
    return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
  }
  else {
    return round($size);
  }
}

// based on file upload demo by CertaiN
// found on http://php.net/manual/en/features.file-upload.php

try {
   
    // Undefined | Multiple Files | $_FILES Corruption Attack
    // If this request falls under any of them, treat it invalid.
    if (
        !isset($_FILES['upfile']['error']) ||
        is_array($_FILES['upfile']['error'])
    ) {
        throw new RuntimeException('Invalid parameters.');
    }

    // Check $_FILES['upfile']['error'] value.
    switch ($_FILES['upfile']['error']) {
        case UPLOAD_ERR_OK:
            break;
        case UPLOAD_ERR_NO_FILE:
            throw new RuntimeException('No file sent.');
        case UPLOAD_ERR_INI_SIZE:
        case UPLOAD_ERR_FORM_SIZE:
            throw new RuntimeException('Exceeded filesize limit.');
        default:
            throw new RuntimeException('Unknown errors.');
    }
	
    // You should also check filesize here.
    if ($_FILES['upfile']['size'] > file_upload_max_size()) {
        throw new RuntimeException('Exceeded filesize limit.');
    }

    // DO NOT TRUST $_FILES['upfile']['mime'] VALUE !!
    // Check MIME Type by yourself.
	// see http://filext.com/faq/office_mime_types.php
	// http://www.ooowiki.de/OpenDocument.html
    $finfo = new finfo(FILEINFO_MIME_TYPE);
    if (false === $ext = array_search(
        $finfo->file($_FILES['upfile']['tmp_name']),
        array(
            'pdf' => 'application/pdf',
            'doc' => 'application/msword',
            'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
			'ppt' => 'application/vnd.ms-powerpoint',
			'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
			'odt' => 'application/vnd.oasis.opendocument.text',
			'odp' => 'application/vnd.oasis.opendocument.presentation'
        ),
        true
    )) {
        throw new RuntimeException('Invalid file format.');
    }

    // You should name it uniquely.
    // DO NOT USE $_FILES['upfile']['name'] WITHOUT ANY VALIDATION !!
    // On this example, obtain safe unique name from its binary data.
	//$filename=sha1_file($_FILES['upfile']['tmp_name']).'.'.$ext;
	$filename=$u->pid.strtolower(preg_replace('/[^a-zA-Z0-9_-]/s', '',$u->name)).$vr->vrid.'.'.$ext;
    if (!move_uploaded_file($_FILES['upfile']['tmp_name'],'./files./'.$filename)) 
		throw new RuntimeException('Failed to move uploaded file.');
    
    $vr->data=$filename;
	$vr->commit();
	include 'tools.inc.php';
	Tools::change_status($vr,$u,'ok');
	header('Location: vorgang.php?vgid='.$vr->vgid);
	

} catch (RuntimeException $e) {

    echo $e->getMessage();

}

?>