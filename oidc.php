<?php

session_start();
if (isset($_SESSION['user'])) {
	session_destroy();
	session_start();
}

require __DIR__ . '/vendor/autoload.php';

use Jumbojett\OpenIDConnectClient;

include 'config.oidc.php';

$oidc = new OpenIDConnectClient(OIDCConfig::$host,OIDCConfig::$clientid,OIDCConfig::$secret);

$oidc->setResponseTypes(array('code'));
$oidc->providerConfigParam(array('auth_endpoint'=>OIDCConfig::$auth_endpoint,'token_endpoint'=>OIDCConfig::$token_endpoint,'userinfo_endpoint'=>OIDCConfig::$uinfo_endpoint));
$oidc->setRedirectURL(OIDCConfig::$redirect);
$oidc->addAuthParam(array('response_mode' => 'form_post'));
$oidc->addScope('profile groups');

try {
	$authresult = $oidc->authenticate();
} catch(Exception $e) {
	echo "Fehler bei der IServ-Authentifizierung: <br />\n".$e;
	exit;
}

if($authresult == true) {
	$userinfo = $oidc->requestUserInfo();
	//echo "IServ-Authentifizierung erfolgreich.<br>";
	$grps=$userinfo->groups;
	$role='no';
	$user=$userinfo->preferred_username;
	
	foreach ($grps as $g) {
		if ($g->act=='admins') $role='1'; // admin
		if ($role=='no' && $g->act=='jahrgang.12') $role='2'; // student
		if ($role=='no' && $g->act=='kollegium') $role='3'; // teacher
	}
	if ($role!='no') {
		$_SESSION['gid']=$role;
		$_SESSION['pid']=$user;
		header( 'Location: desktop.php' );
		exit;
	} else {
		echo 'Du hast keinen Zugriff auf die Prüfungsanträge.';
	}
//	var_dump_pre($userinfo); 
	exit;
}

function var_dump_pre($mixed = null) {
  echo '<pre>';
  var_dump($mixed);
  echo '</pre>';
  return null;
}

