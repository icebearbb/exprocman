<?php

class User {
	
	public $name='';
	public $vorname='';
	public $gid='';
	public $mail='';
	public $pid='';
	public $rfach='';
	public $bfach='';
	
	public function __construct($id,$gid) {
		$this->pid=DB::sqli()->escape_string($id);
		$this->gid=DB::sqli()->escape_string($gid);
		$row=DB::get_assoc_row("SELECT name,vorname FROM person WHERE pid='$this->pid'");
		$this->name=$row['name'];
		$this->vorname=$row['vorname'];
		$this->mail=$this->pid.'@humboldtschule-berlin.eu';
		if ($this->gid==2) {
			$this->rfach=DB::get_value("SELECT fid FROM schuelerfach WHERE pid='$this->pid' AND ref=1");
			$isbfach=DB::get_value("SELECT COUNT(fid) FROM schuelerfach WHERE pid='$this->pid' AND ref=2");
			if ($isbfach==1) 
				$this->bfach=DB::get_value("SELECT fid FROM schuelerfach WHERE pid='$this->pid' AND ref=2");
		}
	}
	
	public function update_ping(){
		DB::query("UPDATE person SET ping=NULL WHERE pid='$this->pid'");
	}
	
	public static function auth_ok() {
		if (isset($_SESSION['pid'])) {
			$u=new User($_SESSION['pid'],$_SESSION['gid']);
			$u->update_ping();
			return $u;
		}
	}

}

?>