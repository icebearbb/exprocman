<?php
	include 'dbinterface.inc.php';
	include 'usermgr.inc.php';
	session_start();
	
	DB::connect();
	$u=User::auth_ok();
	if (!isset($u) || $u->gid!=1) {
		header('Location: logout.php');
		exit;
	}
	
	include 'header.inc.php';
	echo Header::generate ($u);
	include 'menu.inc.php';
	echo Menu::generate ($u,'gpadmin');
	include 'view.inc.php';
	
	echo '<b>Gruppenprüfungen verwalten</b><br><br>';
	
	if (isset($_GET['del'])) {
		$gn=$_GET['del'];
		DB::query("DELETE FROM gpruef WHERE gpnr=$gn");
	}
	
	if (isset($_POST['stu1'])) {
		$s=array();
		$s[0]=DB::esc($_POST['stu1']);
		$s[1]=DB::esc($_POST['stu2']);
		$s[2]=DB::esc($_POST['stu3']);
		$nextg=1+DB::get_value('SELECT MAX(gpnr) FROM gpruef');
		if ($s[2]=='no') {
			DB::query ("INSERT INTO gpruef (gpnr,vgid1,vgid2) VALUES ($nextg,$s[0],$s[1])");
			DB::query ("INSERT INTO gpruef (gpnr,vgid1,vgid2) VALUES ($nextg,$s[1],$s[0])");
		} else {
			DB::query ("INSERT INTO gpruef (gpnr,vgid1,vgid2) VALUES ($nextg,$s[0],$s[1])");
			DB::query ("INSERT INTO gpruef (gpnr,vgid1,vgid2) VALUES ($nextg,$s[1],$s[0])");
			DB::query ("INSERT INTO gpruef (gpnr,vgid1,vgid2) VALUES ($nextg,$s[2],$s[1])");
			DB::query ("INSERT INTO gpruef (gpnr,vgid1,vgid2) VALUES ($nextg,$s[2],$s[0])");
			DB::query ("INSERT INTO gpruef (gpnr,vgid1,vgid2) VALUES ($nextg,$s[0],$s[2])");
			DB::query ("INSERT INTO gpruef (gpnr,vgid1,vgid2) VALUES ($nextg,$s[1],$s[2])");
		}
	}

	$vgids=DB::get_list("SELECT DISTINCT vg.vgid FROM darf d,voraus v,vorgang vg 
				WHERE d.vrid=v.vrid AND v.vgid=vg.vgid");
	$vgs=array();
	$i=0;
	foreach ($vgids as $v) {
		$vgs[$i]=new Vorgang($v,$u);
		$i++;
	}
	
	$gps=DB::get_assoc('SELECT gpnr,vgid1,vgid2 FROM gpruef');
	$gpnr=DB::get_list('SELECT DISTINCT gpnr FROM gpruef');
	
	$opts='<option value="no">-- Bitte wählen! --';
	for ($i=0;$i<count($vgs);$i++) {
		$show=true;
		foreach ($gps as $g) {
			if ($vgs[$i]->vgid==$g['vgid1'] || $vgs[$i]->vgid==$g['vgid2']) {
				$show=false;
				break;
			}
		}
		if ($show)
			$opts.='<option value='.$vgs[$i]->vgid.'>'.$vgs[$i]->get_stud_name().'</option>';
	}
	
	echo '<form action="gpadmin.php" method="POST"><table class="desktop">'.
		'<tr><td>1. Schüler</td><td>2. Schüler</td><td>3. Schüler</td></tr>'.
		'<tr><td><select name="stu1">'.$opts.'</select></td>'.
		'<td><select name="stu2">'.$opts.'</select></td>'.
		'<td><select name="stu3">'.$opts.'</select></td>'.
		'</tr></table>'.
		'<input type="submit" value="Anlegen"></form><br>';
	
	echo 'Bestehende Prüfungen:<br>'.
		'<table class="desktop"><tr><th>Prüfling 1</th><th>Prüfling 2</th><th>Prüfling 3</th><th>Löschen</th></tr>';
	foreach ($gpnr as $gn) {
		// create a list of vgids for this group exam
		$vgids=array();
		foreach ($gps as $g) {
			if (!in_array($g['vgid1'],$vgids) && $g['gpnr']==$gn) $vgids[]=$g['vgid1'];
			if (!in_array($g['vgid2'],$vgids) && $g['gpnr']==$gn) $vgids[]=$g['vgid2'];
		}
		// get names of students involved in this group exam
		echo '<tr>';
		foreach ($vgids as $vgid) {
			foreach ($vgs as $v) {
				if ($v->vgid==$vgid) {
					echo '<td>'.$v->get_stud_name().'</td>';
				}
			}
		}
		if (count($vgids)<3) echo '<td>&nbsp;</td>';
		echo '<td><button onclick="document.location=\'gpadmin.php?del='.$gn.
			'\';">Löschen</button></td></tr>';
	}
	echo '</table>';
	
	include 'footer.inc.php';
	echo Footer::generate ($u);
?>
